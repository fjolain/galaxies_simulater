#! /usr/bin/python3
from processing_multi import launch
from preprocessing import AstresGenerator
from postprocessing import Plotting
from math import cos, sin, pi
import numpy as np

iteration =200
dt = 364

astres = AstresGenerator(dt)
sun = astres.add_sun(1.9891e30)                             # Sun
#astres.add_planet(   330e21,  sun, 359976739e3, 88)         # Mercure
#astres.add_planet(4.8685e24,  sun, 679888899e3, 583)        # Venus
astres.add_planet(5.9736e24,  sun, 939885629e3, 364)        # Earth
#astres.add_planet(641.85e21,  sun, 1429038772e3, 780)       # Mars
init = astres.init_generator()

sim = launch(iteration, dt, 1, init)

for time in sim.keys() :
    print("x :", sim[time][0][1], "y :", sim[time][1][1])

plot = Plotting(sim, 1e14)
plot.show()

