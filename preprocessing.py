import numpy as np
from numpy.random import rand, randn
from math import pi, cos, sin, log10, sqrt
from time import sleep

class MassifAstre :
    
    def __init__(self, mass, x, y, vx, vy) :
        self.mass = mass
        self.x    = x
        self.y    = y
        self.vx   = vx
        self.vy   = vy
            
class AstresGenerator :
    
    def __init__(self, dt) :
        self.M = []
        self.x0 = []
        self.y0 = []
        self.x1 = []
        self.y1 = []
        self.dt = dt

    def add_massif_astre(self, mass, x, y, vx, vy) :
        self.M.append(mass)

        self.x0.append(x)
        self.y0.append(y)
        self.x1.append(x+vx)
        self.y1.append(y+vy)

        return MassifAstre(mass, x, y, vx, vy)

    def add_orbitant_astre(self, mass, massif_astre, radius, revolution, angle) :
        self.M.append(mass)
        
        radius = radius
        x_0 = radius*cos(angle) + massif_astre.x
        y_0 = radius*sin(angle) + massif_astre.y
        
        angle -= 2*pi/revolution*self.dt
        x_1 = radius*cos(angle) + massif_astre.x + massif_astre.vx
        y_1 = radius*sin(angle) + massif_astre.y + massif_astre.vy

        self.x0.append(x_0)
        self.y0.append(y_0)
        self.x1.append(x_1)
        self.y1.append(y_1)

    def add_sun(self, mass) :
        return self.add_massif_astre(mass, 0, 0, 0, 0)

    def add_planet(self, mass, sun, perimeter, revolution) :
        self.add_orbitant_astre(mass, sun, perimeter/2/pi, revolution, 2*pi*rand())


    def add_galaxy(self, x, y, vx, vy, size, npart) :
        black_hole = self.add_massif_astre(8.4e36, x, y, vx, vy)
        phi = (1  + sqrt(5))/2

        for theta in np.arange(0, 3*pi, 6*pi/npart) :
            mass = 1e30 + rand()*1e32
            radius = size*pow(phi, 2*theta/pi -6) + randn()*6e18*theta**1.5
            revolution = 2*pi*sqrt(pow(radius, 3)/(6.67408e-11*black_hole.mass))
            self.add_orbitant_astre(mass, black_hole, radius, revolution, theta)

            mass = 1e30 + rand()*1e32
            radius = size*pow(phi, 2*theta/pi -6) + randn()*6e18*theta**1.5
            revolution = 2*pi*sqrt(pow(radius, 3)/(6.67408e-11*black_hole.mass))
            self.add_orbitant_astre(mass, black_hole, radius, revolution, theta+pi)

        
    def init_generator(self) :
        return {0 : (np.array(self.x0), np.array(self.y0), np.array(self.M)),
                1 : (np.array(self.x1), np.array(self.y1), np.array(self.M))}
