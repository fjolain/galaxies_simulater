# Galaxies simulater

Programme to simulate galaxies by using only Newton laws

## Try it
You can launch directly `./galaxy result.glx`. After 1 hour of comuting you will have a result.glx for raw data results (position and mass astre in the time) and a movie.mp4 movie to show the galaxies dancing !

## Compile
To compile cython tab `python3 setup.py build_ext --inplace`

## Files
* `preprocessing.py` to build initiale conditions i.e. first and second position astre
* `processing_multi.pyx` cython source code for computing programm
* `processing_multi.*.so` and `processing_multi.c` binary compiling programm
* `postprocessing` programm to visualize data. Directly by tkinter if data is not huge. Or by a movie maker to build a mp4 movie.
* `setup.py` a kind of Makefile for cython
* `galaxy.py` and `sun_system.py` example files
