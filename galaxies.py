#! /usr/bin/python3

from processing_multi import launch
from preprocessing import AstresGenerator
from postprocessing import Plotting
from time import time
import pickle
import sys

from math import *

path = sys.argv[1]

def save_obj(obj, name ):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
            

iteration = 4000
dt = 31536e11

astres = AstresGenerator(dt)
astres.add_galaxy(-2e21, -2e21, 2e17, 1e17, 2e21, 400)
astres.add_galaxy( 2e21,  2e21,-2e17,-1e17, 2e21, 400)
sim= astres.init_generator()
print(sim[0][0].size)

t1 = time()
res_multi = launch(iteration, dt, 4, sim)
t2 = time()
print("cython : ", t2-t1)
save_obj(res_multi, path)

plot = Plotting(res_multi, 3e21)
plot.show()
