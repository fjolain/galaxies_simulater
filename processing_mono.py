import numpy as np
import multiprocessing as mp

class Processing :

    def __init__(self, sim) :

        self.M      = np.array(sim[0][2])
        self.x_curr = np.array(sim[1][0])
        self.x_old  = np.array(sim[0][0])
        self.y_curr = np.array(sim[1][1])
        self.y_old  = np.array(sim[0][1])
        self.n = self.x_curr.shape[0]
        

    @staticmethod
    def compute_force_around(i, x, y, n, m) :
        fx = [0]*n
        fy = [0]*n
        remove = []


        for j in range(1, len(x)) :
            rx = x[j] - x[0]
            ry = y[j] - y[0]
            r = pow(rx*rx + ry*ry, 1.5)
            
            
            if r <= 5e28 :
                remove.append((i, i+j))
                f = 0
            else :
                f = 6.67408e-11*m[0]*m[j]/r # 6.67408e-11 = G
            
            fx[i+j] = f*rx
            fy[i+j] = f*ry

        return fx, fy, remove

    def compute_force(self, t) :
        
        res = [Processing.compute_force_around(i, self.x_curr[i:].tolist(), self.y_curr[i:].tolist(), self.n,  self.M[i:].tolist()) for i in range(self.n)]

        F_x = np.array([item[0] for item in res])
        F_y = np.array([item[1] for item in res])

        planets_removing = sum([item[2] for item in res], [])
        if planets_removing != [] :
            F_x, F_y = self.remove_planets(planets_removing, F_x, F_y)

        F_x -= F_x.T
        F_y -= F_y.T
        

        return F_x, F_y
    
    def remove_planets(self, planets_removing, F_x, F_y) :
        
        # Remove planets asked many times
        girls_asked = []
        planets_removing_clean = []
        for i in range(len(planets_removing)) :
            if not planets_removing[i][1] in girls_asked :
                planets_removing_clean.append(tuple(planets_removing[i]))
                girls_asked.append(planets_removing[i][1])

        # Sort list by index girls higher
        planets_removing_sort = []
        n = len(planets_removing_clean)
        for i in range(n) :
            max_value = 0
            index = 0
            for j in range(len(planets_removing_clean)) :
                if planets_removing_clean[j][1] > max_value :
                    index = j
                    max_value = planets_removing_clean[j][1]
            planets_removing_sort.append(tuple(planets_removing_clean[index]))
            del planets_removing_clean[index]
        #planets_removing_sort = planets_removing_sort[::-1]

        #print(planets_removing, "--->", planets_removing_sort)

        for mom, girl in planets_removing_sort :
            self.M[mom] += self.M[girl]
            F_x = np.delete(np.delete(F_x, girl, axis=0), girl, axis=1)
            F_y = np.delete(np.delete(F_y, girl, axis=0), girl, axis=1)

            self.M = np.delete(self.M, girl)
            self.x_curr = np.delete(self.x_curr, girl)
            self.x_old  = np.delete(self.x_old , girl)
            self.y_curr = np.delete(self.y_curr, girl)
            self.y_old  = np.delete(self.y_old , girl)
            self.n -= 1

        return F_x, F_y


    @staticmethod
    def compute_elementary_position(p_n, p_nm1, F, m, dt, i) :
        return 2*p_n - p_nm1 + (dt**2)*F/m
    
    def compute_position(self, time, dt) :
        F_x, F_y = self.compute_force(time)

        x_next = [Processing.compute_elementary_position(
                            self.x_curr[i], self.x_old[i], 
                            np.sum(F_x[i]), self.M[i], dt, i      ) for i in range(self.n)]
                            
        y_next = [Processing.compute_elementary_position(
                            self.y_curr[i], self.y_old[i], 
                            np.sum(F_y[i]), self.M[i], dt,i        ) for i in range(self.n)]

        self.x_old  = self.x_curr
        self.x_curr = np.array(x_next)

        self.y_old  = self.y_curr
        self.y_curr = np.array(y_next)

        return self.x_curr, self.y_curr, self.M


                            

    def launch(self, iteration, dt) :

        res = {0 : (self.x_old, self.y_old, self.M), 1 : (self.x_curr, self.y_curr, self.M)}
        
        for time in range(1, iteration-1) :
            res[time] = self.compute_position(time, dt)
        
        return res
