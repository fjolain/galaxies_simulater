#! /usr/bin/python3

import numpy as np
from time import sleep
from tkinter import *
from math import pi, cos, sin, log10, sqrt
import sys
import pickle
import os
import shutil
import threading

class Plotting :


    def __init__(self, frames, lim) :
        self.frames = frames
        self.iterations = len(frames)
        self.n = frames[0][0].size

        self.radius = 1
        self.height = 800
        self.width = 800*16./9
        self.limits = lim
        self.km2px = -self.height/self.limits/2
        self.circles = []
        
        self.animation = Tk()
        self.canvas = Canvas(self.animation, width=self.width, height=self.height, bg='black')
        self.canvas.pack()

        gradient_color = self.__build_gradient_color(self.frames[0][2])
        self.__build_canvas(0, True)

        #self.animation.mainloop()

    def get_pixels(self, x, y) :
        x_px = self.km2px*x + self.width/2
        y_px = self.height -(self.km2px*y + self.height/2)
        return int(x_px), int(y_px)

    def __build_canvas(self, time, delete=False) :

        if delete :
            self.circles = []
            self.canvas.delete(ALL)

        for i in range(self.n):
            x, y = self.get_pixels(self.frames[time][0][i], self.frames[time][1][i])
            r = self.radius
            if delete :
                m = self.frames[time][2][i]
                #gradient_color = self.__build_gradient_color(self.frames[time][2])
                circle = self.canvas.create_oval(x-r, y-r, x+r, y+r, fill='#FFFFFF')#gradient_color(m))
                self.circles.append(circle)
            else :
                self.canvas.coords(self.circles[i], (x-r, y-r, x+r, y+r))

    def __build_gradient_color(self, m) :
        cmin  = log10(np.min(m))
        cmax  = log10(np.max(m))
        cmean = log10(np.median(m))

        return lambda cm : Plotting.__gradient_color_proto(cm, cmin, cmax, cmean)
    
    @staticmethod
    def __gradient_color_proto(m, cmin, cmax, cmean) :
        m = log10(m)
        b = (cmean - m) / (cmean - cmin)
        r = (m - cmean) / (cmax - cmean)
        if m < cmean : 
            g = (m - cmin) / (cmean - cmin)
        else :
            g = (cmax - m) / (cmax - cmean)

        b = max(b, 0)
        b = min(b, 1)
        r = max(r, 0)
        r = min(r, 1)
        g = max(g, 0)
        g = min(g, 1)

        return '#%02x%02x%02x' % (int(r*255), int(g*255), int(b*255))



    def __animate(self, time):

        n_read = self.frames[time][0].size
        if n_read != self.n :
            self.n = n_read
            self.__build_canvas(time, True)
        else :
            self.__build_canvas(time, True)
        self.canvas.update()

    def show(self) :
        for time in range(self.iterations) :
            self.__animate(time)
            sleep(0.016)
    

def load_obj(name):
    with open(name, 'rb') as f:
        return pickle.load(f)


if __name__ == '__main__' :
    path = sys.argv[1]
    huge_num = sys.argv[2].split('e')
    lim = int(huge_num[0])
    if len(huge_num) > 1 :
        lim = lim*pow(10, int(huge_num[1]))
    plot = Plotting(load_obj(path), lim)
    plot.show()
