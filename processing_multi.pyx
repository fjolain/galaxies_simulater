import cython
cimport cython

from cython.parallel import prange
import numpy as np
cimport numpy as np
DTYPE = np.double
ctypedef np.double_t DTYPE_t


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
cpdef launch(int iteration, long dt, int nb_proc, dict sim) :
    
    cdef double rx                                                              # y distance composante
    cdef double ry                                                              # x distance composante
    cdef double r                                                               # distance betwwen to astre power by 1.5
    cdef double rswcd                                                           # schwarzschild radius power by 1.5
    cdef double f                                                               # elementary pseudo magnitude gravition force 
    cdef double G = 6.67408e-11                                                 # graviation constante
    cdef double schwarzschild = 1.48513e-27                                     # constante to compute Scharwschild radius
    cdef double dt2 = pow(dt, 2)                                                # time step power two
    cdef double rm_tmp                                                          # tempo variable used during removing astre algo
    
    cdef int size_data = len(sim[1][0])                                         # global size data
    cdef int n = size_data                                                      # current size data
    cdef int i, j                                                               # indice for primary and second indice main loop
    cdef int size_remove                                                        # current index in removing and keeping array
    cdef int i_kepp, i_remove                                                   # index for removing loop
    
    cdef np.ndarray[DTYPE_t, ndim=1] x_curr = np.array(sim[1][0])               # array x astres current position
    cdef np.ndarray[DTYPE_t, ndim=1] x_old  = np.array(sim[0][0])               # array x astres old position
    cdef np.ndarray[DTYPE_t, ndim=1] y_curr = np.array(sim[1][1])               # array y astres current position
    cdef np.ndarray[DTYPE_t, ndim=1] y_old  = np.array(sim[0][1])               # array y astres old position
    cdef np.ndarray[DTYPE_t, ndim=1] x_next = np.empty(size_data)               # array x astres next position. Using as tempo swap array
    cdef np.ndarray[DTYPE_t, ndim=1] y_next = np.empty(size_data)               # array y astres next position. Using as tempo swap array
    cdef np.ndarray[DTYPE_t, ndim=1] m      = np.array(sim[0][2])               # array mass astres
    cdef np.ndarray[DTYPE_t, ndim=2] f_x = np.empty((size_data, size_data))     # matrix containing all f elementary force on x dimension
    cdef np.ndarray[DTYPE_t, ndim=2] f_y = np.empty((size_data, size_data))     # matrix constainng all f elementary force on y dimension

    
    cdef dict res = {0 : (x_old, y_old, m), 1 : (x_curr, y_curr, m)}            # dictionary as output simulation resul


    for time in range(1, iteration-1) :
        # Clear local data
        f_x = np.zeros((size_data, size_data))
        f_y = np.zeros((size_data, size_data))

        # Compute astre at next position
        for i in prange(0, n, nogil=True, num_threads=nb_proc) :#, sichedule='static') :

            # Compute scharzschild radius powered by 1.5
            rswcd = m[i]*schwarzschild**1.5
            
            # Gravition force is symetric, just need to compute upper matrix
            for j in range(i+1, n) :
                if i != j :
                    rx = x_curr[j] - x_curr[i]
                    ry = y_curr[j] - y_curr[i]
                    r = (rx*rx + ry*ry)**1.5
                    
                    # Removing astre if to close or in scharzschild radius
                    if r <= 1e55 or r < rswcd:
                        f = 1.7e200 # Set max double value as a tag
                    else :
                        f = G*m[j]*m[i]/r

                    f_x[i, j] = f*rx
                    f_y[i, j] = f*ry
        
        # record removing data by tag placed below
        keeping, removing = np.where(f_x > 1e200)
        remove_size = np.where(f_x > 1e200)[0].size
        
        # Then apply a anti symetric operation to compute all gravitions forces
        f_x -= f_x.T
        f_y -= f_y.T
    
        # Compute next position             2        .----
        # Come from dynamic newton law :   d X    1   \
        #                                 ----- = --  |   F
        #                                    2    m   /
        #                                  dt        .----
        
        x_next = 2*x_curr - x_old + dt2*np.sum(f_x, axis=1)/m
        y_next = 2*y_curr - y_old + dt2*np.sum(f_y, axis=1)/m

        x_old  = x_curr
        y_old  = y_curr
    
        x_curr = x_next
        y_curr = y_next

        # Removing astre
        if remove_size > 0 :
            for i in range(0, remove_size) :
                i_keep = keeping[i]
                i_remove = removing[i]
                # addition mass
                m[i_keep] = m[i_keep] + m[i_remove]

                # Delete astre i_remove in arrays : Put astre n-1 at i_remove indice. decrement n size array
                m[i_remove]      =      m[n-1]
                x_curr[i_remove] = x_curr[n-1]
                x_old[i_remove]  =  x_old[n-1]
                y_curr[i_remove] = y_curr[n-1]
                y_old[i_remove]  =  y_old[n-1]
                n = n-1
                print("new n=", n)


        res[time] = (x_curr[:n], y_curr[:n], m[:n])
        print str(time*100./iteration) + "% done"

    return res
